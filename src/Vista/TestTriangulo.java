/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Triangulo;

/**
 *
 * @author David Cordón 02220131004 y Steven Valderrama 02220131006
 */
public class TestTriangulo {

    public static void validar(Triangulo t1, Triangulo t2) {
        if (t1.equals(t2)) {
            System.out.println("son iguales area= " + t1.getArea());
        } else {
            System.out.println("no son iguales t1 area= " + t1.getArea() + " t2 area= " + t2.getArea());
        }
    }

    public static void main(String[] args) {
        Triangulo t1 = new Triangulo(5, 4, 7, 7, "equilatero");
        Triangulo t2 = new Triangulo(5, 4, 8, 8, "equilatero");

        System.out.println(t1.getBase());
        System.out.println(t1.getAltura());
        System.out.println(t1.getLado1());
        System.out.println(t1.getLado2());
        System.out.println(t1.getTipo());
        System.out.println(t1.toString());
        System.out.println("---------------------------------------------------------------");

        validar(t1, t2);
        t1.setBase(7);
        t1.setAltura(8);
        t1.setLado1(6);
        t1.setLado2(9);
        t1.setTipo("isósceles");
        System.out.println("---------------------------------------------------------------");

        System.out.println(t1.getBase());
        System.out.println(t1.getAltura());
        System.out.println(t1.getLado1());
        System.out.println(t1.getLado2());
        System.out.println(t1.getTipo());
        System.out.println(t1.toString());
        System.out.println("---------------------------------------------------------------");

        validar(t1, t2);
    }
}
