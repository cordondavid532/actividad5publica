/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.ListadoTriangulo;
import Modelo.Triangulo;

/**
 *
 * @author cordo
 */
public class TestListado_MIO {

    public static void main(String[] args) {
        Triangulo t1 = new Triangulo(4, 4, 7, 7, "equilatero");
        Triangulo t2 = new Triangulo(3, 6, 6, 8, "equilatero");
        Triangulo t3 = new Triangulo(5, 4, 2, 7, "equilatero");
        Triangulo t4 = new Triangulo(4, 2, 8, 8, "equilatero");
        Triangulo t5 = new Triangulo(6, 8, 4, 7, "equilatero");

        ListadoTriangulo nL = new ListadoTriangulo();
        nL.insertar(t1);
        nL.insertar(t2);
        nL.insertar(t3);
        nL.insertar(t4);
        nL.insertar(t5);
        
        System.out.println("listado triangulos: ");
        System.out.println(nL.toString());
        System.out.println("---------------------------------------------------------------------------------");
        
        System.out.println("elemnto menot: ");
        System.out.println(nL.getMenor().toString()+ "Area= "+nL.getMenor().getArea());
        System.out.println("---------------------------------------------------------------------------------");
        
        System.out.println("Listado menores");
        int valorMaximo=6;
        ListadoTriangulo newlistado=nL.getMenores(valorMaximo);
        System.out.println(newlistado.toString());
    }
}
