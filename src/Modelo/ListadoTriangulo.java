/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.LinkedList;

/**
 * Clase que modela un listado de Triangulos
 *
 * @author David Cordón 02220131004 y Steven Valderrama 02220131006
 */
public class ListadoTriangulo {

    // Es una lista de Triangulos: 
    private LinkedList<Triangulo> myLista = new LinkedList();

    /**
     * Construcctor vacio de la clase ListadoTriangulo
     */
    public ListadoTriangulo() {
    }

    /**
     * Método que obtiene el la lista de triangulos que estan en el atributo
     * myLista
     *
     * @return una lista de objetos triangulo
     */
    public LinkedList<Triangulo> getMyLista() {
        return myLista;
    }

    /**
     * Método que actualiza la lista el atributo myLista
     *
     * @param myLista
     */
    public void setMyLista(LinkedList<Triangulo> myLista) {
        this.myLista = myLista;
    }

    /**
     * Método que muestra al usuario los triangulos que tiene el objeto
     *
     * @return un String que contiene todos los datos de los triangulos
     */
    @Override
    public String toString() {
        String msg = " ";
        int contador = 1;
        for (Triangulo triangulo : myLista) {
            msg += (contador++) + ". " + triangulo.toString() + "\t";
        }
        return msg;
    }

    /**
     * Método que agrega un nuevo triangulo a la lista del atributo myLista
     *
     * @param nuevo es el unevo triagulo que se va a agregar a la lista
     */
    public void insertar(Triangulo nuevo) {
        this.myLista.add(nuevo);
    }

    /**
     * Método retorna el triangulo con menor area
     *
     * @return el truangulo con menot area del atributo myLista
     */
    public Triangulo getMenor() {
        // menor = myLista[0]
        Triangulo menor = this.myLista.get(0);
        for (int i = 1; i < this.myLista.size(); i++) {
            Triangulo dos = this.myLista.get(i);
            int c = menor.compareTo(dos);
                if (c > 0) {
                    menor = dos;
                }
            
        }
        return menor;
    }
    
    /**
     * Método que crea un nuevo listado con los elemento que son menores al valor dado 
     * 
     * @param valor_maximo un float que determina el valor maximo del area del triangulo para poder ser insertado en este listado
     * @return un listado 
     */
    public ListadoTriangulo getMenores(float valor_maximo)
    {
        ListadoTriangulo newListado=new ListadoTriangulo();
        for (int i = 0; i < this.myLista.size(); i++) {
            if (this.myLista.get(i).getArea()<valor_maximo) {
                newListado.insertar(this.myLista.get(i));
            }
        }
        return newListado;
    }
}
