/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.util.Objects;

/**
 * Clase que modela un triangulo
 *
 * @author David Cordón 02220131004 y Steven Valderrama 02220131006
 */
public class Triangulo implements Comparable<Triangulo> {

    private float base;
    private float altura;
    private float lado1;
    private float lado2;
    private String tipo;

    //Requisito 1
    /**
     * contructor vacio de la clase triangulo
     */
    public Triangulo() {
    }

    /**
     * constructor con parametros de la clase Triangulo
     *
     * @param base un float que almacena la medida de la base del triangulo
     * @param altura un float que almacena la medida de la altura del triangulo
     * @param lado1 un float que almacena la medida del lado 1 en el triangulo
     * @param lado2 un float que almacena la medida del lado 2 en el triangulo
     * @param tipo un String que almacena el tipo de triangulo que es
     */
    public Triangulo(float base, float altura, float lado1, float lado2, String tipo) {
        this.base = base;
        this.altura = altura;
        this.lado1 = lado1;
        this.lado2 = lado2;
        this.tipo = tipo;
    }

    //Requisito 2
    /**
     * Método que obtiene el valor del atributo base
     *
     * @return un float que contiene el valor de base del triangulo
     */
    public float getBase() {
        return base;
    }

    /**
     * Método que actualiza el valor del atributo base
     *
     * @param base que contiene el nuevo valor del atributo this.base
     */
    public void setBase(float base) {
        this.base = base;
    }

    /**
     * Método que obtiene el valor del atributo altura
     *
     * @return un float que contiene el valor de la altura del triangulo
     */
    public float getAltura() {
        return altura;
    }

    /**
     * Método que actualiza el valor del atributo altura
     *
     * @param altura que contiene el nuevo valor del atributo this.altura
     */
    public void setAltura(float altura) {
        this.altura = altura;
    }

    /**
     * Método que obtiene el valor del atributo lado1
     *
     * @return un float que contiene el valor de lado1 del triangulo
     */
    public float getLado1() {
        return lado1;
    }

    /**
     * Método que actualiza el valor del atributo lado1
     *
     * @param lado1 que contiene el nuevo valor del atributo this.lado1
     */
    public void setLado1(float lado1) {
        this.lado1 = lado1;
    }

    /**
     * Método que obtiene el valor del atributo lado2
     *
     * @return un float que contiene el valor de lado2 del triangulo
     */
    public float getLado2() {
        return lado2;
    }

    /**
     * Método que actualiza el valor del atributo lado2
     *
     * @param lado2 que contiene el nuevo valor del atributo this.lado2
     */
    public void setLado2(float lado2) {
        this.lado2 = lado2;
    }

    /**
     * Método que obtiene el valor del atributo tipo
     *
     * @return un String que contiene el tipo de triangulo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Método que actualiza el valor del atributo tipo
     *
     * @param tipo que contiene el nuevo valor del atributo this.tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    //Requisito 3
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.tipo);
        return hash;
    }

    /**
     * Método que compara si dos triangulos son iguales en base a su tipo de
     * triangulo y el area
     *
     * @param obj es el otro triangulo con el que se va a hacer la comparacion
     * @return un boolean que dice si son iguales o no
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Triangulo other = (Triangulo) obj;
        if (this.tipo.equals(other.getTipo())) {
            float myArea = this.getArea();
            float areaOther = other.getArea();
            if (myArea == areaOther) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Método que se usa para sacar el area del triangulo
     *
     * @return un float que contiene el valor del area del triangulo
     */
    public float getArea() {
        return (this.base * this.altura) / 2;
    }

    //requisito 4
    /**
     * Método que obtiene la informacion de los atributos del triangulo
     *
     * @return un String que contiene toda la informacion
     */
    @Override
    public String toString() {
        return "Triangulo{" + "base=" + base + ", altura=" + altura + ", lado1=" + lado1 + ", lado2=" + lado2 + ", tipo=" + tipo + '}';
    }

    @Override
    public int compareTo(Triangulo o) {
        float myArea = this.getArea();
        float otherArea = o.getArea();
        float restaArea = myArea - otherArea;
        if (restaArea == 0) {
            return 0;
        } else if (restaArea < 0) {
            return -1;
        } else {
            return 1;
        }
    }
}
